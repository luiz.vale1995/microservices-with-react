import { FirestoreService } from "../../../Common/Model/FirestoreService";
import { IPerson } from "./People";

export class PersonService extends FirestoreService<IPerson> {
    constructor() {
        super('people')
    }
}