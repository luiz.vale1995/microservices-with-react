import firebase from "firebase";
import { Guid } from "guid-typescript";


import { IDatabaseModelBase } from './DatabaseModelBase'

export abstract class FirestoreService<T extends IDatabaseModelBase> {
    constructor(private collection: string) {}
    

    public Save(item: T): Promise<T> {
        return new Promise<T>(async (coll) => {

            item.ServerID = Guid.create().toString();

            await firebase.firestore().collection(this.collection).doc(item.ServerID).set(item)

            coll(item)
        })
    }

    public async Get(id: string): Promise<T> {

        const qry = await firebase.firestore().collection(this.collection).doc(id).get();

        return <T>qry.data();
    }

    public async GetAll(): Promise<T[]> {

        const qry = await firebase.firestore().collection(this.collection).get()

        const items: T[] = new Array<T>();

        qry.forEach(item => {
            items.push(<T>item.data());
        });
        
        return items

    }
}