import { FirestoreService } from "../../../Common/Model/FirestoreService";
import { IAddress } from "./Addresses";

export class AddressesService extends FirestoreService<IAddress> {
    constructor(){
        super('addresses');
    }
}